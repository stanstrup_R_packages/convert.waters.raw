# convert.waters.raw

Converts files from Waters .raw format to mzData.
MassLynx need to be installed and masswolf need to be in path.
This works around the problem of properly converting Waters data. See the supplementary material [here](http://dx.doi.org/10.1007/s00216-013-6954-6).

## Installation
Install the devtools package and run:

```r
install.packages("devtools")
devtools::install_git("https://gitlab.com/R_packages/convert.waters.raw.git")
```
